#include <imgui.h>
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include "../Build/src/shader1.h"
#include "../Build/src/stb_image.h"
//#include "../Build/src/glm/glm.hpp"
//#include "../Build/src/glm/gtc/matrix_transform.hpp"
//#include "../Build/src/glm/gtc/type_ptr.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow* window);
void drawTri()
{
	glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, 0);
}
void drawTri(float x, float y, float z, float d, Shader ourShader)
{
	glm::mat4 model = glm::mat4(1.0f);
	GLfloat ver3 = 1.0f * (GLfloat)sqrt(6) / 3;
	GLfloat ver1 = 1.0f * (GLfloat)sqrt(3) / 6;
	model = glm::translate(model, glm::vec3(x/2, y*ver3, z*ver1));
	model = glm::scale(model, glm::vec3(d, d, d));
	ourShader.setMat4("model", model);
	glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, 0);
}
void drawPir(float x, float y, float z, float d, int n, Shader ourShader)
{

	if (n == 1)
	{
		drawTri(x, y, z, d, ourShader);
	}
	else
	{
		drawPir(x - d / 2, y, z - d / 2, d / 2, n - 1, ourShader);
		drawPir(x + d / 2, y, z - d / 2, d / 2, n - 1, ourShader);
		drawPir(x, y, z + d, d / 2, n - 1, ourShader);
		drawPir(x, y + d/2, z, d / 2, n - 1, ourShader);
	}
		

}

const unsigned int screen_width = 800;
const unsigned int screen_height = 600;



int main()
{
	static float obrot_X = 0.0f;
	static float obrot_Y = 0.0f;
	int poziom_rekurencji = 1;
	static float oddalenie = 1.0f;
	static float color[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
	bool tylko_krawedzie = false;

	glfwInit();
	const char* glsl_version = "#version 430";
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow* window = glfwCreateWindow(screen_width, screen_height, "Piramida Sierpinskiego", nullptr, nullptr);
	glfwMakeContextCurrent(window); //glowny kontekst dla watku jest window
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress); //ladujemy adresy funkcji OpenGl

	//glViewport(0, 0, 800, 600);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glEnable(GL_DEPTH_TEST);

	Shader ourShader("shader.vs", "shader.fs");

	//WIERZCHOLKI I KONFIGURACJA + BUFFORY
	GLfloat ver1 = 1.0f * (GLfloat)sqrt(3) / 6;
	GLfloat ver2 = ver1 * 2;
	GLfloat ver3 = 1.0f * (GLfloat)sqrt(6) / 3;
	std::cout << "ver1 =" << ver1 << " ver2 =" << ver2 << " ver3 =" << ver3 << std::endl;

	GLfloat vertices1[] = {
		 0.5f, -0.5f,  0.5f,	 1.0f, 1.0f,
		-0.5f,  0.5f,  0.5f,	 1.0f, 0.0f,
		 0.5f,  0.5f, -0.5f,	 0.0f, 0.0f,
		-0.5f, -0.5f, -0.5f,	 0.0f, 1.0f
	};
	GLfloat vertices[] = {
		-0.5f,  0.0f, -ver1,	 1.0f, 1.0f,
		 0.5f,  0.0f, -ver1,	 1.0f, 0.0f,
		 0.0f,  0.0f,  ver2,	 0.0f, 0.0f,
		 0.0f,  ver3,  0.0f,	 0.0f, 1.0f
	};

	GLuint indices[] = {
		0,1,2,
		1,3,2,
		3,0,2,
		0,3,1
	};
	
	
	

	GLuint VBO, VAO, EBO;
	//wypelniamy VBO danymi wierzcholkow
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
	//ustawiamy wskazniki atrybutu wierzcholka
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);
	//kolory
	//glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	//glEnableVertexAttribArray(1);
	//tekstury
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	int width, height, nrChannels;
	stbi_set_flip_vertically_on_load(true);
	unsigned char* data = stbi_load("stone.jpg", &width, &height, &nrChannels, 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "nie mozna zaladowac tekstury" << std::endl;
	}
	stbi_image_free(data);


	ourShader.use();
	ourShader.setInt("texture", 1);

	//IMGUI
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init(glsl_version);
	ImGui::StyleColorsDark();


	while (!glfwWindowShouldClose(window))
	{
		//input
		processInput(window);
		//render
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//IMGUI
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();
		
		glActiveTexture(GL_TEXTURE);
		glBindTexture(GL_TEXTURE_2D, texture);

		ourShader.use();

		//macierze modelu, widoku i projekcji
		glm::mat4 view = glm::mat4(1.0f);
		glm::mat4 projection = glm::mat4(1.0f);

		view = glm::translate(view, glm::vec3(0.0f, 0.0f, oddalenie*-3.0f));
		view = glm::rotate(view, glm::radians(1.0f * obrot_X), glm::vec3(1.0f, 0.0f, 0.0f));
		view = glm::rotate(view, glm::radians(1.0f * obrot_Y), glm::vec3(0.0f, 1.0f, 0.0f));
		projection = glm::perspective(glm::radians(45.0f), (float)screen_width/(float)screen_height, 0.1f, 100.0f);

		ourShader.setMat4("view", view);
		ourShader.setMat4("projection", projection);

		glBindVertexArray(VAO);

		if(tylko_krawedzie)
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		/*
		for (unsigned int i = 0; i < 2; i++)
		{
			glm::mat4 model = glm::mat4(1.0f);


			model = glm::translate(model, glm::vec3(trans_X * 1.0f * i, trans_Y * 1.0f * i, trans_Z * 1.0f * i));
			model = glm::scale(model, glm::vec3(1.0f / (i + 1), 1.0f / (i + 1), 1.0f / (i + 1)));
			ourShader.setMat4("model", model);

			drawTri();
			glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, 0);
		}*/
		drawPir(0, 0, 0, 1, poziom_rekurencji, ourShader);
		if(tylko_krawedzie)
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		//okno IMGUI
		
		ImGui::Begin("okno Dear ImGui");
		ImGui::SliderFloat("X", &obrot_X, -180.0f, 180.0f);
		ImGui::SliderFloat("Y", &obrot_Y, -180.0f, 180.0f);
		ImGui::SliderFloat("oddalenie", &oddalenie, 0.01f, 1.0f);
		ImGui::SliderInt("poziom rekurencji", &poziom_rekurencji, 1, 8);
		ImGui::ColorEdit3("color", color);
		ourShader.setFloat("color1", color[0]);
		ourShader.setFloat("color2", color[1]);
		ourShader.setFloat("color3", color[2]);
		ourShader.setFloat("color4", color[3]);
		ImGui::Checkbox("tylko krawedzie", &tylko_krawedzie);
		ImGui::End();
		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());



		//glfw
		glfwSwapBuffers(window);
		glfwPollEvents(); //sprawdza czy zostal wcisniety klawisz
	}


	glfwTerminate();
	return 0;
}


void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}
