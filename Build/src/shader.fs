#version 330 core  
 
out vec4 color;

in vec2 TexCoord;
in vec3 FragPos;
in vec3 Normal;


uniform float color1;
uniform float color2;
uniform float color3;
uniform float color4;

uniform sampler2D ourTexture;
uniform vec3 lightPos;
uniform vec3 lightColor;
uniform vec3 objectColor;

void main()  
{  
float ambientStrength = 0.1;
vec3 ambient = ambientStrength * lightColor;
vec3 norm = normalize(Normal);
vec3 lightDir = normalize(lightPos - FragPos);
float diff = max(dot(norm, lightDir), 0.0);
vec3 diffuse = diff * lightColor;

vec4 surfaceColor = texture(ourTexture, TexCoord);
        
vec3 wynik = (ambient + diffuse) * objectColor;
color = vec4(wynik, 1.0)*surfaceColor;
    
}
